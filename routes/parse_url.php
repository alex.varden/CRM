<?php

  $page_depth = 0 ;

  $url = '*';

  if (isset($_GET['url'])) {
      $url = explode('/', $_GET['url']);
      $page_depth = count($url);

      foreach ($url as $x => $value) {
          if ($value == '') {
              unset($url[$x]);
          }
      }
  }

  $PAGE = $url[0];

  if (is_array($url) && $page_depth > 1) {
      unset($url[0]);
      $params = $url;
  }
