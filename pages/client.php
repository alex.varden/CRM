

<div class="row placeholders">
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="text-align:left">
    <h1><?php print $client->name; ?></h1>
    <hr/>
  <table class="table table-striped">
    <tbody>
      <tr>
        <td>Address</td>
        <td><?php echo  $client->address ?></td>
      </tr>
     <?php

      foreach ($client->attributes as $attribute) {
          print "
          <tr>
            <td>{$attribute['attribute']}</td>
            <td>{$attribute['value']}</td>
          </tr>";
      }

    ?>




    </tbody>
  </table>

   <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add Info</button>




  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Info</h4>
        </div>
        <div class="modal-body">

          <form action="<?=route('/add_attribute')?>" method="POST">
            <div class="form-group">
              <label for="title">Title:</label>
              <input type="title" class="form-control" name="title">
            </div>

            <input type="hidden" name="token"   value="<?php echo $CSRF_Token; ?>">
            <input type="hidden" name="company" value="<?php echo $client->name; ?>">

            <div class="form-group">
              <label for="value">Value:</label>
              <input type="text" name="value" class="form-control" id="value">
            </div>


        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-success">Save</button>

          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
         </form>

        </div>
      </div>

    </div>
  </div>


</div>
</div>
