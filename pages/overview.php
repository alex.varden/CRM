

<div class="row placeholders">
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="text-align:left">
        <h1>Clients</h1>

<?php
        if (count($clients) == 0) {
            print "Please add some Clients ! <br/> <a href='".route('/add')."'   ><button type=\"button\" class=\"btn btn-primary\"> Add New</button></a>";
            exit();
        }

?>

        <table class="table">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Company Name</th>
                    <th>Company Address</th>
                    <th>Actions</th>
                 </tr>
            </thead>
            <tbody>

            <?php


                    foreach ($clients as $client) {
                        print <<<HTML

                        <tr>
                            <td>{$client['id']}</td>
                            <td>{$client['name']}</td>
                            <td>{$client['address']}</td>
                            <td>
                                      <a href='client/{$client["name"]}' ><button type="button" class="btn btn-primary">view  </button></a>
                                       <a href='remove/{$client["name"]}'   ><button type="button" class="btn btn-danger"> delete</button></a>
                            </td>
                        </tr>

HTML;
                    }
            ?>

            </tbody>
        </table>

 <a href='<?=route('add')?>'   ><button type="button" class="btn btn-primary"> Add New</button></a>




    </div>
</div>
