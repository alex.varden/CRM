<?php


    $error = false;

    if (isset($_POST['user']) && isset($_POST['password'])) {

        //Sanitsise input
        $user     = filter_var($_POST['user'], FILTER_SANITIZE_STRING);
        $password = filter_var($_POST['password'], FILTER_SANITIZE_STRING);


        if (Authenticate_User($user, $password)) {
            $_SESSION['user'] = $user;
            goToRoute('/');
        } else {
            $error = 'Incorrect Attempt';
        }
    }

    include'layout/head.php';

?>

    <div class="container" id="login">

<?php    if ($error) {
    print  "<div class='alert alert-danger' role='alert'> $error </div>";
} ?>


      <form action="<?=route('/login')?>" method="POST" class="form-signin">
        <h2 class="form-signin-heading">Please sign in</h2>

        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="text" name='user' id="inputuser"  class="form-control"   placeholder="User Name" required autofocus>

        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" name='password' id="inputPassword" class="form-control" placeholder="Password" required>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->
