

<div class="row placeholders">
  <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="text-align:left">
    <h1>Add Client</h1>


    <form action="<?=route('/add')?>" method="POST">

      <div class="form-group">
        <label for="name">Company Name:</label>
        <input type="text" class="form-control" name="name">
      </div>



      <div class="form-group">
        <label for="name">Company Address:</label>
        <input type="text" class="form-control" name="address">
      </div>

        <input type="hidden" name="token" value="<?php echo $CSRF_Token; ?>">


      <button type="submit" class="btn btn-primary">
        <i class="fa fa-save" style="margin-right: 10px;"></i> Save
      </button>

    </form>


</div>
</div>
