<?php

include 'db.php';

class client
{
    private $db;


    public $id;
    public $name;
    public $address;


    public function __construct()
    {
        $this->db = new mysql_db(CONFIG['db']['local']);
    }

    public function loadCompany($info)
    {
        $this->id       = $info['id'];
        $this->name    = $info['name'];
        $this->address = $info['address'];

        $this->getCompanytAttributes();
    }

    public function getClientByName($name)
    {
        $company = new company();
        $data = $company->getByName($name);

        if (count($data)>0) {
            $this->loadCompany($data[0]);
        } else {
            exit('cannot find : '.$name);
        }
    }

    public function listAll()
    {
        $company = new company();
        return $company->listAll();
    }

    public function remove()
    {
        $company = new company();
        return $company->remove($this->id);

        $attribute = new attribute();
        $attribute->removeAll($this->id);
    }



    public function addAttribute($name, $value)
    {
        $attribute = new attribute();
        $attribute->create($this->id, $name, $value);
    }


    public function getCompanytAttributes()
    {
        $attribute = new attribute();
        $this->attributes = $attribute->getByEntity($this->id);
        return $this->attributes;
    }
}
