<?php


class mysql_db
{
    public function __construct($credentials)
    {
        $this->db        = $credentials['db'];
        $this->host    = $credentials['host'];
        $this->username = $credentials['username'];
        $this->password = $credentials['password'];

        $this->connect();
    }

    public function connect()
    {
        $this->connection = new mysqli(
                                        $this->host,
                                        $this->username,
                                        $this->password,
                                        $this->db
                                      );


        //Output any connection error
        if ($this->connection->connect_error) {
            die('Error : ('. $this->connection->connect_errno .') '. $this->connection->connect_error);
        }

        return true;
    }


    public function query($sql)
    {
        $this->results = $this->connection->query($sql) or die('Unable to execute query. '. mysqli_error($this->connection)."Query:".$sql);
    }


    public function returnArray($sql)
    {
        $this->query($sql);

        $return = array();

        if ($this->results->num_rows > 0) {
            while ($row = $this->results->fetch_assoc()) {
                $return[] = $row;
            }
        }
        return $return;
    }



    public function update($id, $values, $table)
    {
        $lookupKeys = $this->lookupDataTypes(array_keys($values), $table);
        $i = 0;

        $sql = "UPDATE $table SET ";


        foreach ($values as $key => $value) {
            if ($i > 0) {
                $sql .= ' , ' ;
            }
            $i++;

            if (!isset($lookupKeys[$key])) {
                die($key.' : column cannot be found');
            }

            switch ($lookupKeys[$key]) {
                 case 'varchar':
                    $value = ' "'.$value.'" ';
                break;

            }

            $sql .= "$key = $value";
        }

        $sql .= " WHERE id = $id ";

        $this->query($sql);
    }



    public function lookupDataTypes($lookupKeys, $table)
    {
        $lookupKeys =  implode("' OR COLUMN_NAME = '", $lookupKeys);
        $lookupKeys = "(COLUMN_NAME = '".$lookupKeys."')";

        $SQL =  "SELECT
                     COLUMN_NAME as 'key' ,
                     DATA_TYPE   as 'value'
                    FROM
                         INFORMATION_SCHEMA.COLUMNS
                 WHERE
                          TABLE_SCHEMA = '{$this->db}'
                    AND TABLE_NAME = '$table'
                 AND $lookupKeys";


        return $this->returnMergedArray($SQL);
    }



    public function returnMergedArray($sql)
    {
        $this->query($sql);

        $return = array();

        if ($this->results->num_rows > 0) {
            while ($row = $this->results->fetch_assoc()) {
                if (isset($row['value']) && isset($row['key'])) {
                    $return[$row['key']] = $row['value'];
                }
            }
        }
        return $return;
    }
}
