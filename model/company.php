<?php


class company
{
	function __construct()
	{
		$this->db = new mysql_db(CONFIG['db']['local']);
	}

	public function getById($id)
	{
	
		$sql  = "SELECT * FROM company where Id = $id";

		return $this->db->returnArray($sql);

	}


	public function getByName($name)
	{
	
		$sql  = "SELECT * FROM company where name like '$name' limit 1";

		return $this->db->returnArray($sql);

	}

	public function listAll()
	{
 		$sql  = "SELECT * FROM company";

		return $this->db->returnArray($sql);
	}	
  
  
	public function create( $name , $address)
	{
		 	$sql	=	"INSERT INTO company 
		 					(name , address) 
						 VALUES
						 	( '$name' , '$address' )";
 
			return $this->db->query($sql);
 	}

 	
  
	public function remove($id)
	{
		 	$sql	=	"delete from company where id = $id";
 
			return $this->db->query($sql);
 	}

 	 

 	public function update( $id , $name = null , $address = null )
	{

		$parameters = [
						'name'	  => $name,
						'address' => $address,
		   			  ];


		// // remove null values
		foreach ($parameters as $key => $value) {
 			if(is_null($value)){
 				unset($parameters[$key]);
 			}
 		}


 		$this->db->update($id,$parameters , 'company');

 	}
}