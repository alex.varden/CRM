<?php

$relative_position = '';
for ($i=1; $i < $page_depth ; $i++) { 
  $relative_position .= '../';
}

echo <<<HTML
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
 
    <title>CRM</title>

    <!-- jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- Bootstrap core CSS -->
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="{$relative_position}css/dashboard.css" rel="stylesheet">
    <link rel="stylesheet" href="{$relative_position}css/font-awesome.css" rel="stylesheet" type="text/css">
 
  </head>

  <body>
 
 
    <script type="text/javascript">
      
      function page (url) {

        console.log(  $('a[href="'+url+'"]').parent('li') );
        $('a[href='+url+']').parent('li').addClass('active');

      }

    </script>

HTML;


  
?>



<script>  page('<?php echo $_GET['url']; ?>'); </script>