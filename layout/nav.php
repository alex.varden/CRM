
 <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" style="font-size: 30px; padding: 10px;
          "> <i class="fa fa-cube" style="color:#7114e4 " ></i> CRM</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">

        </div>
      </div>
    </nav>


    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">


          <?php

            foreach ($pages as $name => $link) {
                if ($link == 'group') {
                    echo "<li  class='link_group' >".ucfirst($name)."</li>";
                    continue;
                }
                $name_ = str_replace(' ', '_', $name);
                echo "<li id='link_$name_' class='link_$link' ><a href='".route($link)."'>".ucfirst($name)."</a></li>";
            }

          ?>

          </ul>

          <ul class="nav nav-sidebar">
               <?php  print '<li class="link_logout danger"><a href="'.$relative_position.'logout">Logout</a></li>'; ?>
          </ul>

   </div>
   </div>
   </div>
