/*
 Navicat MySQL Data Transfer

 Source Server         : Vagrant
 Source Server Type    : MySQL
 Source Server Version : 50632
 Source Host           : localhost
 Source Database       : CRM

 Target Server Type    : MySQL
 Target Server Version : 50632
 File Encoding         : utf-8

 Date: 05/09/2017 21:26:29 PM
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `company`
-- ----------------------------
DROP TABLE IF EXISTS `company`;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `company`
-- ----------------------------
BEGIN;
INSERT INTO `company` VALUES ('17', 'Microsoft', 'Aviator Way, Wythenshawe, Manchester M22 5TG'), ('18', 'Boohoo', '49 mabfeild, 12391293');
COMMIT;

-- ----------------------------
--  Table structure for `company_attributes`
-- ----------------------------
DROP TABLE IF EXISTS `company_attributes`;
CREATE TABLE `company_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `company_attributes`
-- ----------------------------
BEGIN;
INSERT INTO `company_attributes` VALUES ('1', 'Phone', '0128242792874', '12'), ('2', 'Email', 'alexvarden@gmail.com', '12'), ('3', 'phone', '071231290530453', '7'), ('4', 'Phone', '01273891273981', '17'), ('5', 'Email', 'alexvarden@microsoft.com', '17'), ('6', 'Diretor', 'harry dixon', '18');
COMMIT;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('1', 'user', '*2470C0C06DEE42FD1618BB99005ADCA2EC9D1E19', 'user@email.com');
INSERT INTO `user` VALUES ('1', 'alexvarden', '*39CA46D43EE96A32C72623F424BF9F0BD9D4FBBA', 'alexvarden@gmail.com');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
