<?php

session_start();

include 'config.php';
include 'utilities.php';
include 'model/client.php';
include 'model/company.php';
include 'model/attributes.php';


//NAV PAGES
$pages = [
        'clients' => 'group', /// group
        'All'      => '/' ,
        'Add New' => 'add'
];



include 'routes/routes.php';
